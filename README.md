# iDigitalMedium

iDigitalMedium (IDM) was built with WordPress using Beaver Builder, serving as their primary website and volunteer recruitment location.

In this solo project, I worked with the site owner to scope out his needs during an initial discovery phase, which was used to develop and implement an information architecture plan.

My main goal was to simplify the site visitors experience and highlight IDM's most impactful content. 

The original site's landing page had over 40 navigation options and calls to action which were reduced to less than 20 on the new site.

A full width, mobile-first layout was choosen to optimize the mobile-vistors experience, which represent the majority of site traffic now.

Beaver Builder is a highly effective tool for rapidly building responsive, reusable custom layouts in WordPress.


[iDigitalMedium.com site](https://idigitalmedium.com)

[Beaver Builder Page Builder](https://www.wpbeaverbuilder.com)
